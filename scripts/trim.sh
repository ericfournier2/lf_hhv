for i in raw/*.fastq.gz
do
    module load trimmomatic
    sample=`basename $i .fastq.gz | sed -e 's/Wes_Moorman_//'`
    mkdir -p output/jobs
    script=output/jobs/$sample.trim.sh
    cat <<EOF > $script
#!/bin/bash
#SBATCH --account=def-stbil30
#SBATCH --time=2:00:00
#SBATCH --mem=32G
#SBATCH --cpus-per-task=8

    module load trimmomatic

    mkdir -p output/trim/$sample
    java -jar $EBROOTTRIMMOMATIC/trimmomatic-0.36.jar SE -threads 8 $i output/trim/$sample.fastq.gz HEADCROP:10 TRAILING:20 SLIDINGWINDOW:20:24 MINLEN:20
EOF

    sbatch --export=sample=$sample --job-name=$sample.TRIM -o $script.stdout -e $script.stderr -D `pwd` $script 
    
done
