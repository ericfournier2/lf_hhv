for i in raw/*.fastq.gz
do
    sample=`basename $i .fastq.gz | sed -e 's/Wes_Moorman_//'`
    mkdir -p output/jobs
    script=output/jobs/$sample.star_human.sh
    cat <<EOF > $script
#!/bin/bash
#SBATCH --account=def-stbil30
#SBATCH --time=6:00:00
#SBATCH --mem=128G
#SBATCH --cpus-per-task=8

    module load star/2.6.1a

    mkdir -p output/star_human/$sample
    STAR --runMode alignReads \
    --genomeDir /project/6001942/ProstateOmega3/ref/STAR_index \
    --readFilesIn $i \
    --runThreadN 8 \
    --readFilesCommand zcat \
    --outStd Log \
    --outReadsUnmapped Fastx \
    --outSAMtype BAM Unsorted \
    --outFileNamePrefix output/star_human/$sample/ \
    --outFilterScoreMinOverLread 0 \
    --outFilterMatchNminOverLread 0 \
    --outFilterMatchNmin 0 \
    --limitGenomeGenerateRAM 100000000000 \
    --limitIObufferSize 4000000000 \
    --outFilterMultimapNmax 10000


EOF

    sbatch --export=sample=$sample --job-name=$sample.STAR_HUMAN -o $script.stdout -e $script.stderr -D `pwd` $script 
    
done
