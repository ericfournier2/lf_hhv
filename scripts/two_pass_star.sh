for i in raw/*.fastq.gz
do
    sample=`basename $i .fastq.gz | sed -e 's/Wes_Moorman_//'`
    mkdir -p output/$sample
    mkdir -p output/jobs
    script=output/jobs/$sample.star.sh
    cat <<EOF > $script
#!/bin/bash
#SBATCH --account=def-stbil30
#SBATCH --time=6:00:00
#SBATCH --mem=128G
#SBATCH --cpus-per-task=8

module load star/2.6.1a
    STAR --runMode alignReads \
    --genomeDir ref/STAR_index_no_overhang \
    --readFilesIn $i \
    --runThreadN 4 \
    --readFilesCommand zcat \
    --outStd Log \
    --outSAMunmapped Within \
    --outSAMtype BAM Unsorted \
    --outFileNamePrefix output/$sample/STAR \
    --limitGenomeGenerateRAM 100000000000 \
    --limitIObufferSize 4000000000
EOF

    sbatch --export=sample=$sample --job-name=$sample.STAR -o $script.stdout -e $script.stderr -D `pwd` $script 
    
done




