#!/bin/bash
#SBATCH --account=rrg-adroit-ab
#SBATCH --time=6:00:00
#SBATCH --mem=128G
#SBATCH --cpus-per-task=8

mkdir -p ref/STAR_index

module load star/2.6.1a

STAR --runThreadN 8 \
    --runMode genomeGenerate \
    --genomeDir ref/STAR_index_no_hoverhang \
    --genomeFastaFiles input/AF157706.1.fasta


