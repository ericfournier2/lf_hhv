#for i in raw/*.fastq.gz
for i in output/trim/*.fastq.gz
do
    sample=`basename $i .fastq.gz | sed -e 's/Wes_Moorman_//'`

    mkdir -p output/jobs
    script=output/jobs/$sample.hisat2-trim.sh
    
    outdir=output/hisat2-trim
    mkdir -p $outdir    
    
    cat <<EOF > $script
#!/bin/bash
#SBATCH --account=def-stbil30
#SBATCH --time=6:00:00
#SBATCH --mem=128G
#SBATCH --cpus-per-task=8

module load hisat2
module load samtools

hisat2 --novel-splicesite-outfile $outdir/$sample.splices \
       --max-intronlen 10000 \
       --downstream-transcriptome-assembly \
       --summary-file $outdir/$sample.summary \
       --new-summary \
       --met-file $outdir/$sample.met \
       -x ref/hisat2/no_ref \
       -U $i \
       -S $outdir/$sample.sam
samtools view -bh $outdir/$sample.sam > $outdir/$sample.unsorted.bam
samtools sort $outdir/$sample.unsorted.bam > $outdir/$sample.sorted.bam
samtools index $outdir/$sample.sorted.bam

rm $outdir/$sample.unsorted.bam $outdir/$sample.sam
EOF

    sbatch --export=sample=$sample --job-name=$sample.HISAT2 -o $script.stdout -e $script.stderr -D `pwd` $script 
    
done
