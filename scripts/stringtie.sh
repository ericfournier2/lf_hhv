for i in output/hisat2/*.bam
do
    sample=`basename $i .sorted.bam`
    mkdir -p output/stringtie
    mkdir -p output/jobs
    script=output/jobs/$sample.stringtie.sh
    cat <<EOF > $script
#!/bin/bash
#SBATCH --account=def-stbil30
#SBATCH --time=2:00:00
#SBATCH --mem=32G
#SBATCH --cpus-per-task=1

module load stringtie

stringtie $i -o output/stringtie/$sample.summary -f 0 -m 50 -M 0

EOF

    sbatch --export=sample=$sample --job-name=$sample.stringtie -o $script.stdout -e $script.stderr -D `pwd` $script 
    
done
