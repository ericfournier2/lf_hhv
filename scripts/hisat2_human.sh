for i in raw/*.fastq.gz
do
    sample=`basename $i .fastq.gz | sed -e 's/Wes_Moorman_//'`
    mkdir -p output/$sample
    mkdir -p output/jobs
    script=output/jobs/$sample.hisat2_human.sh
    cat <<EOF > $script
#!/bin/bash
#SBATCH --account=def-stbil30
#SBATCH --time=6:00:00
#SBATCH --mem=128G
#SBATCH --cpus-per-task=8

module load hisat2
module load samtools

mkdir -p output/hisat2_human
hisat2 --summary-file output/hisat2_human/$sample.summary \
       --new-summary \
       --un-gz output/hisat2_human/$sample.unaligned.fastq.gz \
       -x /scratch/efournie/grch38/genome \
       -U $i \
       -S output/hisat2_human/$sample.sam
samtools view -bh output/hisat2_human/$sample.sam > output/hisat2_human/$sample.unsorted.bam
samtools sort output/hisat2_human/$sample.unsorted.bam > output/hisat2_human/$sample.sorted.bam
samtools index output/hisat2_human/$sample.sorted.bam

rm output/hisat2_human/$sample.unsorted.bam output/hisat2_human/$sample.sam
EOF

    sbatch --export=sample=$sample --job-name=$sample.HISAT2_HUMAN -o $script.stdout -e $script.stderr -D `pwd` $script 
    
done
